#include "speller/speller.h"
#include <QGuiApplication>
#include <QSurfaceFormat>

int main(int argc, char *argv[]) {

  QGuiApplication app(argc, argv);

  QSurfaceFormat format;
  format.setSwapInterval(1);
  format.setRenderableType(QSurfaceFormat::OpenGL);
  format.setProfile(QSurfaceFormat::CoreProfile);
  format.setVersion(3, 3);

  // Set the window up
  Speller spell(argc, argv);
  spell.window->setFormat(format);
  spell.window->show();
  spell.start();

  return app.exec();
}
