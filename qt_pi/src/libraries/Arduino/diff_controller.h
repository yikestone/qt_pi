typedef struct {
  double TargetTicksPerFrame;
  long Encoder;
  long PrevEnc;

  int PrevInput;
  
  int ITerm;
  int Kp = 45;
  int Kd = 0;
  int Ki = 5;
  int Ko = 3;
  int base_pwm = 70;
  long output;
}

SetPointInfo;

SetPointInfo leftPID, rightPID;


unsigned char moving = 0;

void resetPID(){
   leftPID.TargetTicksPerFrame = 0.0;
   leftPID.Encoder = readEncoder(LEFT);
   leftPID.PrevEnc = leftPID.Encoder;
   leftPID.output = 0;
   leftPID.PrevInput = 0;
   leftPID.ITerm = 0;

   rightPID.TargetTicksPerFrame = 0.0;
   rightPID.Encoder = readEncoder(RIGHT);
   rightPID.PrevEnc = rightPID.Encoder;
   rightPID.output = 0;
   rightPID.PrevInput = 0;
   rightPID.ITerm = 0;
}

void doPID(SetPointInfo * p) {
  long Perror;
  long output;
  int input;

  input = p->Encoder - p->PrevEnc;
  Perror = p->TargetTicksPerFrame - input;
  p->ITerm += (p->Ki) * Perror;
  output = ((p->Kp) * Perror + (p->Kd) * (input - p->PrevInput) + p->ITerm) / (p->Ko);
  
  if(p->TargetTicksPerFrame > 0) output += p->base_pwm;
  else if(p->TargetTicksPerFrame < 0) output -= p->base_pwm;
  
  p->PrevEnc = p->Encoder;
  
  if (output >= MAX_PWM)
    output = MAX_PWM;
  else if (output <= -MAX_PWM)
    output = -MAX_PWM;
  else

  p->output = output;
  p->PrevInput = input;
}

void updatePID() {

  leftPID.Encoder = readEncoder(LEFT);
  rightPID.Encoder = readEncoder(RIGHT);

  if (!moving){
    if (leftPID.PrevInput != 0 || rightPID.PrevInput != 0) resetPID();
    return;
  }

  if(leftPID.TargetTicksPerFrame != 0) doPID(&leftPID);
  if(rightPID.TargetTicksPerFrame != 0) doPID(&rightPID);
  
  setMotorSpeeds(leftPID.output, rightPID.output);
}
