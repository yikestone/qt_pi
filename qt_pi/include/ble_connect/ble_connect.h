#ifndef BLE_CONNECT
#define BLE_CONNECT
#include "gattlib.h"
#include <glib.h>
#include <mcrypt.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

enum Channels { AF3 = 0, T7 = 1, Pz = 2, T8 = 3, AF4 = 4 };

struct insight_data {
  double uV[5];
  struct timespec ts;
};
#ifdef __cplusplus
extern "C" {
#endif
extern int insight_init(const char *a, const char *b);
extern int insight_destroy();
extern int insight_get_status();
extern int insight_start_notif();
extern int insight_stop_notif();
extern int insight_write_to_buffer(int len, struct insight_data *buf);
extern int insight_buffer_status();
extern int insight_stop_write_to_buffer();
extern int insight_version();
#ifdef __cplusplus
}
#endif
#endif
