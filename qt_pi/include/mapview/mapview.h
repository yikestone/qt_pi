#ifndef MAPVIEW
#define MAPVIEW

#include "vertex.h"

#include <QDebug>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions_3_1>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLWindow>
#include <QString>
#include <chrono>
#include <thread>
#include <unistd.h>

class QOpenGLShaderProgram;

class MapView : public QOpenGLWindow, protected QOpenGLFunctions_3_1 {
  Q_OBJECT
public:
  explicit MapView(QWidget *parent = 0);
  ~MapView();

  void start_blinking();
  void stop_blinking();
  void toggle_blinking();

protected:
  GLsizei count;
  GLint startingElements[4] = {0, 4, 8, 12};
  GLsizei counts[4] = {4, 4, 4, 4};

  void initializeGL() override;
  void paintGL() override;
  void resizeGL(int, int) override;
  void teardownGL();
  void printContextInformation();

private:
  QOpenGLBuffer m_vertex;
  QOpenGLVertexArrayObject m_object;
  QOpenGLShaderProgram *m_program;

  QOpenGLBuffer map_vertex;
  QOpenGLVertexArrayObject map_object;
  QOpenGLShaderProgram *map_program;
  QOpenGLTexture *map_texture;
  Vertex *vertices;
  int vertices_len;

  volatile int sleep_control = 1;
  volatile int blinking;
  int map_redraw;

  std::thread *t;

  void run();
  void keyPressEvent(QKeyEvent *key) override;
  void mousePressEvent(QMouseEvent *event) override;
signals:
  void update_GUI();
  void keyPress(QKeyEvent *key);
  void map_loaded();
  void mouse_press(QMouseEvent *);
private slots:
  void load_texture(int, Vertex *, QImage *);
};

#endif
