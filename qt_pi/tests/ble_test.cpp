#include "ble_connect/ble_connect.h"
#include <unistd.h>
int main() {
  const char *a = "EF:BD:39:4E:08:49";
  const char *b = "81072f41-9f3d-11e3-a9dc-0002a5d5c51b";

  insight_init(a, b);
  insight_start_notif();

  struct insight_data *buf =
      (struct insight_data *)malloc(sizeof(struct insight_data) * 1000);

  int len = 1000;

  insight_write_to_buffer(len, buf);

  while (insight_buffer_status() == 1)
    sleep(0);

  // sleep(5);

  printf("Stopping notif %d \n", insight_stop_notif());

  for (int i = 0; i < len; i++) {
    for (int j = 0; j < 5; j++)
      printf("%f ", buf[i].uV[j]);
    printf("\n");
  }

  printf("Destroy called %d\n", insight_destroy());
  printf("%d\n", insight_version());
  return 0;
}
