# EEG controlled wheel chair prototype
This repository is for qt_pi robot, which is based on shared control. It requires minimal EEG input to traverse from current location to a point in map.

Latest code is in 'temp_branch' branch. Embedded firmware, ROS serial communication node and EEG headset interface code is completed. Whereas as GUI(using Open GL and QT) is partially complete.

Final aim of this project is to enable traversal inside a house requiring only selection of goal by user through EEG interface. A basic simulator which builds a 2D grid map is also provided.

It is using gmapping temporarily but finally it will be based on 3D SLAM.

This repository is under development.

Gazebo sim model:

![](https://i.imgur.com/5SYYqbc.png)
![](https://i.imgur.com/WaGheMV.png)

Real life model:

![](https://i.imgur.com/PEMa60J.jpg)
![](https://i.imgur.com/FBoS0ch.jpg)
